#!/bin/bash
# Purpose: create org's sharepoint drive

drive="$1"
org="Bit-a-Bit"
odhome="${HOME}/${org}/${drive}"
drive="$2"
drivename="od-${drive}"
driveconfig="od-${drive}.config"
dockerrootdir=$(docker info | sed -n 's/Docker Root Dir:.\(\/.*\).*/\1/p' | tr -d '[:space:]')

firstRun='-d'
docker inspect ${driveconfig} > /dev/null || { docker volume create ${driveconfig}; firstRun='-it'; }
docker inspect ${drivename} > /dev/null && docker rm -f ${drivename}
sudo cp assets/od-${drive}.config ${dockerrootdir}/volumes/${driveconfig}/_data/config
docker run $firstRun --restart unless-stopped --name "${drive}" -v "${driveconfig}:/onedrive/conf" -v "${odhome}:/onedrive/data" driveone/onedrive:latest
