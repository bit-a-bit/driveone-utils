#!/bin/bash
# Purpose: create org's local directories

org="Bit-a-Bit"
orghome="${HOME}/${org}"

# diretory for the personal OD
mkdir "${orghome}/Personal"
# directories as functional areas
mkdir "${orghome}/Dirección"
mkdir "${orghome}/OP"
mkdir "${orghome}/Bit-admin"
mkdir "${orghome}/Finanzas"
mkdir "${orghome}/Ventas"
mkdir "${orghome}/Marketing"
mkdir "${orghome}/RRHH"
mkdir "${orghome}/TI"
mkdir "${orghome}/Soporte"
mkdir "${orghome}/Academia"
# other directories
mkdir "${orghome}/Apps"
mkdir "${orghome}/Nebula"
